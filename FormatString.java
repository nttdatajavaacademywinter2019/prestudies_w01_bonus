package anke.awacademy;

import java.util.Scanner;


// App that format strings that are wraped with "_*_" and "#*#"
// "_*_" -> all letters of the string will be in upper case
// "#*#" -> all letters of the string will be in lower case

public class FormatString {

    public static void formatString() {

        Scanner formatScanner = new Scanner(System.in);

        System.out.println("Bitte Text eingeben, der formatiert werden soll:");
        String input = formatScanner.nextLine();

        String[] upper = input.split("_");
        for (int i = 0; i < upper.length; i++) {
            if (i % 2 != 0) {
                upper[i] = upper[i].toUpperCase();
            }
        }

        String[] lower = String.join("", upper).split("#");
        for (int i = 0; i < lower.length; i++) {
            if (i % 2 != 0) {
                lower[i] = lower[i].toLowerCase();
            }
        }

        System.out.println(String.join("", lower));
    }
}